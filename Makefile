COMPILER = g++
FILESYSTEM_FILES = filesystem.cc
FLAGS = -O3 -Wall -fno-operator-names -g
LIBS = -lpthread

all: build

build: $(FILESYSTEM_FILES)
	$(COMPILER) $(FLAGS) $(FILESYSTEM_FILES) -o filesystem `pkg-config fuse --cflags --libs` $(LIBS)

clean:
	rm filesystem

