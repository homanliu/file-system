// Built-in Libraries
#include <stdio.h>
#include <fuse.h>
#include <unistd.h>
#include <sys/types.h>

// Header files
#include "fuse_function.hpp"
#include "config.hpp"

using namespace std;

int fs_getattr(const char *path, struct stat *st){
    return 0;
}

int fs_readdir(const char *path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    return 0;
}

int fs_read(const char *path, char *buffer, size_t size, off_t offset, struct fuse_file_info *fi){
    return 0;
}
