#ifndef _FUSE_FUNCTION_H_
#define _FUSE_FUNCTION_H_

int fs_getattr (const char *, struct stat *);
int fs_readlink (const char *, char *, size_t);
int fs_getdir (const char *, fuse_dirh_t, fuse_dirfil_t);
int fs_mknod (const char *, mode_t, dev_t);
int fs_mkdir (const char *, mode_t);
int fs_unlink (const char *);
int fs_rmdir (const char *);
int fs_symlink (const char *, const char *);
int fs_rename (const char *, const char *);
int fs_link (const char *, const char *);
int fs_chmod (const char *, mode_t);
int fs_chown (const char *, uid_t, gid_t);
int fs_truncate (const char *, off_t);
int fs_utime (const char *, struct utimbuf *);
int fs_open (const char *, struct fuse_file_info *);
int fs_read (const char *, char *, size_t, off_t, struct fuse_file_info *);
int fs_write (const char *, const char *, size_t, off_t, struct fuse_file_info *);
int fs_statfs (const char *, struct statvfs *);
int fs_flush (const char *, struct fuse_file_info *);
int fs_release (const char *, struct fuse_file_info *);
int fs_fsync (const char *, int, struct fuse_file_info *);
int fs_setxattr (const char *, const char *, const char *, size_t, int);
int fs_getxattr (const char *, const char *, char *, size_t);
int fs_listxattr (const char *, char *, size_t);
int fs_removexattr (const char *, const char *);
int fs_opendir (const char *, struct fuse_file_info *);
int fs_readdir (const char *, void *, fuse_fill_dir_t, off_t, struct fuse_file_info *);
int fs_releasedir (const char *, struct fuse_file_info *);
int fs_fsyncdir (const char *, int, struct fuse_file_info *);
void fs_init (struct fuse_conn_info *conn);
void fs_destroy (void *);
int fs_access (const char *, int);
int fs_create (const char *, mode_t, struct fuse_file_info *);
int fs_ftruncate (const char *, off_t, struct fuse_file_info *);
int fs_fgetattr (const char *, struct stat *, struct fuse_file_info *);
int fs_lock (const char *, struct fuse_file_info *, int cmd, struct flock *);
int fs_utimens (const char *, const struct timespec tv[2]);
int fs_bmap (const char *, size_t blocksize, uint64_t *idx);
int fs_ioctl (const char *, int cmd, void *arg, struct fuse_file_info *, unsigned int flags, void *data);
int fs_poll (const char *, struct fuse_file_info *, struct fuse_pollhandle *ph, unsigned *reventsp);
int fs_write_buf (const char *,struct fuse_bufvec *buf, off_t off, struct fuse_file_info *);
int fs_read_buf (const char *,struct fuse_bufvec **bufp, size_t size, off_t off, struct fuse_file_info *);
int fs_flock (const char *,struct fuse_file_info *,int op);
int fs_fallocate (const char *, int, off_t, off_t, struct fuse_file_info *);

#endif