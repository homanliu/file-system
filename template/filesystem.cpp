#define FUSE_USE_VERSION 30

#include <iostream>
#include <fuse.h>

#include "fuse_function.hpp"

using namespace std;

static struct fs_operations: public fuse_operations {
	fs_operations(){
		getattr    = fs_getattr;
		read	   = fs_read;
		readdir    = fs_readdir;
	}
} operations;

int main(int argc, char **argv){
	return fuse_main(argc, argv, &operations, NULL);
}

