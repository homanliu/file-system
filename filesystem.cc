/*
  Big Brother File System
  Copyright (C) 2012 Joseph J. Pfeiffer, Jr., Ph.D. <pfeiffer@cs.nmsu.edu>

  This program can be distributed under the terms of the GNU GPLv3.
  See the file COPYING.

  This code is derived from function prototypes found /usr/include/fuse/fuse.h
  Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>
  His code is licensed under the LGPLv2.
  A copy of that code is included in the file fuse.h
  
  The point of this FUSE filesystem is to provide an introduction to
  FUSE.  It was my first FUSE filesystem as I got to know the
  software; hopefully, the comments in this code will help people who
  follow later to get a gentler introduction.

  This might be called a no-op filesystem:  it doesn't impose
  filesystem semantics on top of any other existing structure.  It
  simply reports the requests that come in, and passes them to an
  underlying filesystem.  The information is saved in a logfile named
  fsfs.log, in the directory from which you run fsfs.
*/

#define FUSE_USE_VERSION 26

#include <iostream>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include "filesystem.hh"

using namespace std;

//  All the paths I see are relative to the root of the mounted
//  filesystem.  In order to get to the underlying filesystem, I need to
//  have the mountpoint.  I'll save it away early on in main(), and then
//  whenever I need a path for something I'll call this to construct
//  it.
static void fs_fullpath(char fpath[PATH_MAX], const char *path){
    strcpy(fpath, FS_DATA->rootdir);
    strlcat(fpath, path, PATH_MAX); // ridiculously long paths will
				    // break here

    // printf("fs_fullpath:  rootdir = \"%s\", path = \"%s\", fpath = \"%s\"\n", FS_DATA->rootdir, path, fpath);
}

int check_retstat(int retstat){
    if(retstat < 0)
        retstat = -errno;
    return retstat;
}

///////////////////////////////////////////////////////////
//
// Prototypes for all these functions, and the C-style comments,
// come from /usr/include/fuse.h
//
/** Get file attributes.
 *
 * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
 * ignored.  The 'st_ino' field is ignored except if the 'use_ino'
 * mount option is given.
 */
int fs_getattr(const char *path, struct stat *statbuf){
    int retstat;
    char fpath[PATH_MAX];
    
    printf("\nfs_getattr(path=\"%s\", statbuf=0x%08x)\n", path, statbuf);
    fs_fullpath(fpath, path);

    retstat = lstat(fpath, statbuf);

    printf("fs_getattr return %d\n", retstat);
    if(fpath[strlen(fpath) - 1] != '/')
	fprintf(stderr, "file size: %ld\n", statbuf->st_size);
    
    return check_retstat(retstat);
}

/** Read the target of a symbolic link
 *
 * The buffer should be filled with a null terminated string.  The
 * buffer size argument includes the space for the terminating
 * null character.  If the linkname is too long to fit in the
 * buffer, it should be truncated.  The return value should be 0
 * for success.
 */
// Note the system readlink() will truncate and lose the terminating
// null.  So, the size passed to to the system readlink() must be one
// less than the size passed to fs_readlink()
// fs_readlink() code by Bernardo F Costa (thanks!)
int fs_readlink(const char *path, char *link, size_t size){
    int retstat;
    char fpath[PATH_MAX];
    
    printf("\nfs_readlink(path=\"%s\", link=\"%s\", size=%lu)\n", path, link, size);
    fs_fullpath(fpath, path);

    retstat = readlink(fpath, link, size - 1);

    if (retstat >= 0) {
	    link[retstat] = '\0';
	    retstat = 0;
	    printf("link=\"%s\"\n", link);
    }
    
    return check_retstat(retstat);
}

/** Create a file node
 *
 * There is no create() operation, mknod() will be called for
 * creation of all non-directory, non-symlink nodes.
 */
// shouldn't that comment be "if" there is no.... ?
int fs_mknod(const char *path, mode_t mode, dev_t dev){
    int retstat = 0;
    char fpath[PATH_MAX];

    printf("\nfs_mknod(path=\"%s\", mode=0%3o, dev=%lu)\n", path, mode, dev);
    fs_fullpath(fpath, path);
    
    // On Linux this could just be 'mknod(path, mode, dev)' but this
    // tries to be be more portable by honoring the quote in the Linux
    // mknod man page stating the only portable use of mknod() is to
    // make a fifo, but saying it should never actually be used for
    // that.
    if (S_ISREG(mode)) {
	    retstat = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
	    if (retstat >= 0)
	        retstat = close(retstat);
    } 
    else if (S_ISFIFO(mode))
	retstat = mkfifo(fpath, mode);
    else
	retstat = mknod(fpath, mode, dev);
   
    return check_retstat(retstat);
}

/** Create a directory */
int fs_mkdir(const char *path, mode_t mode){
    int retstat;
    char fpath[PATH_MAX];
    
    printf("\nfs_mkdir(path=\"%s\", mode=0%3o)\n", path, mode);
    fs_fullpath(fpath, path);

    retstat = mkdir(fpath, mode);
    printf("mkdir return %d\n", retstat);

    return check_retstat(retstat);
}

/** Remove a file */
int fs_unlink(const char *path){
    int retstat;
    char fpath[PATH_MAX];
    
    printf("fs_unlink(path=\"%s\")\n", path);
    fs_fullpath(fpath, path);

    retstat = unlink(fpath);
    printf("unlink return %d\n", retstat);

    return check_retstat(retstat);
}

/** Remove a directory */
int fs_rmdir(const char *path){
    int retstat;
    char fpath[PATH_MAX];
    
    printf("fs_rmdir(path=\"%s\")\n", path);
    fs_fullpath(fpath, path);

    retstat = rmdir(fpath);
    printf("rmdir return %d\n", retstat);

    return check_retstat(retstat);
}

/** Create a symbolic link */
// The parameters here are a little bit confusing, but do correspond
// to the symlink() system call.  The 'path' is where the link points,
// while the 'link' is the link itself.  So we need to leave the path
// unaltered, but insert the link into the mounted directory.
int fs_symlink(const char *path, const char *link){
    char flink[PATH_MAX];
    
    // printf("\nfs_symlink(path=\"%s\", link=\"%s\")\n", path, link);
    fs_fullpath(flink, link);

    return check_retstat(symlink(path, flink));
}

/** Rename a file */
// both path and newpath are fs-relative
int fs_rename(const char *path, const char *newpath){
    char fpath[PATH_MAX];
    char fnewpath[PATH_MAX];
    
    // printf("\nfs_rename(fpath=\"%s\", newpath=\"%s\")\n", path, newpath);
    fs_fullpath(fpath, path);
    fs_fullpath(fnewpath, newpath);

    return check_retstat(rename(fpath, fnewpath));
}

/** Create a hard link to a file */
int fs_link(const char *path, const char *newpath){
    char fpath[PATH_MAX], fnewpath[PATH_MAX];
    
    // printf("\nfs_link(path=\"%s\", newpath=\"%s\")\n", path, newpath);
    fs_fullpath(fpath, path);
    fs_fullpath(fnewpath, newpath);

    return check_retstat(link(fpath, fnewpath));
}

/** Change the permission bits of a file */
int fs_chmod(const char *path, mode_t mode){
    char fpath[PATH_MAX];
    
    printf("\nfs_chmod(fpath=\"%s\", mode=0%03o)\n", path, mode);
    fs_fullpath(fpath, path);

    return check_retstat(chmod(fpath, mode));
}

/** Change the owner and group of a file */
int fs_chown(const char *path, uid_t uid, gid_t gid){
    char fpath[PATH_MAX];
    
    printf("\nfs_chown(path=\"%s\", uid=%d, gid=%d)\n", path, uid, gid);
    fs_fullpath(fpath, path);

    return check_retstat(chown(fpath, uid, gid));
}

/** Change the size of a file */
int fs_truncate(const char *path, off_t newsize){
    char fpath[PATH_MAX];
    
    // printf("\nfs_truncate(path=\"%s\", newsize=%lld)\n", path, newsize);
    fs_fullpath(fpath, path);

    return check_retstat(truncate(fpath, newsize));
}

/** Change the access and/or modification times of a file */
/* note -- I'll want to change this as soon as 2.6 is in debian testing */
int fs_utime(const char *path, struct utimbuf *ubuf){
    char fpath[PATH_MAX];
    
    printf("\nfs_utime(path=\"%s\", ubuf=0x%08x)\n", path, ubuf);
    fs_fullpath(fpath, path);

    return check_retstat(utime(fpath, ubuf));
}

/** File open operation
 *
 * No creation, or truncation flags (O_CREAT, O_EXCL, O_TRUNC)
 * will be passed to open().  Open should check if the operation
 * is permitted for the given flags.  Optionally open may also
 * return an arbitrary filehandle in the fuse_file_info structure,
 * which will be passed to all file operations.
 *
 * Changed in version 2.2
 */
int fs_open(const char *path, struct fuse_file_info *fi){
    int retstat = 0;
    int fd;
    char fpath[PATH_MAX];
    
    printf("\nfs_open(path\"%s\", fi=0x%08x)\n", path, fi);
    fs_fullpath(fpath, path);
    
    // if the open call succeeds, my retstat is the file descriptor,
    // else it's -errno.  I'm making sure that in that case the saved
    // file descriptor is exactly -1.
    fd = open(fpath, fi->flags);
    if (fd < 0){
        perror("open");
	    retstat = -errno;
    }
	
    fi->fh = fd;
    return check_retstat(retstat);
}

/** Read data from an open file
 *
 * Read should return exactly the number of bytes requested except
 * on EOF or error, otherwise the rest of the data will be
 * substituted with zeroes.  An exception to this is when the
 * 'direct_io' mount option is specified, in which case the return
 * value of the read system call will reflect the return value of
 * this operation.
 *
 * Changed in version 2.2
 */
// I don't fully understand the documentation above -- it doesn't
// match the documentation for the read() system call which says it
// can return with anything up to the amount of data requested. nor
// with the fusexmp code which returns the amount of data also
// returned by read.
int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    printf("pread(header\"%lu\")\n", fi->fh);
    return check_retstat(pread(fi->fh, buf, size, offset));
}

/** Write data to an open file
 *
 * Write should return exactly the number of bytes requested
 * except on error.  An exception to this is when the 'direct_io'
 * mount option is specified (see read operation).
 *
 * Changed in version 2.2
 */
// As  with read(), the documentation above is inconsistent with the
// documentation for the write() system call.
int fs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    printf("pwrite(header\"%lu\")\n", fi->fh);
    return check_retstat(pwrite(fi->fh, buf, size, offset));
}

/** Get file system statistics
 *
 * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * Replaced 'struct statfs' parameter with 'struct statvfs' in
 * version 2.5
 */
int fs_statfs(const char *path, struct statvfs *statv){
    int retstat;
    char fpath[PATH_MAX];
    
    printf("\nfs_statfs(path=\"%s\", statv=0x%08x)\n", path, statv);
    fs_fullpath(fpath, path);
    
    // get stats for underlying filesystem
    retstat = statvfs(fpath, statv);
    
    return check_retstat(retstat);
}

/** Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.  It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 */
int fs_release(const char *path, struct fuse_file_info *fi){
    printf("\nfs_release(path=\"%s\", fi=0x%08x)\n", path, fi);

    // We need to close the file.  Had we allocated any resources
    // (buffers etc) we'd need to free them here as well.
    return check_retstat(close(fi->fh));
}

/** Synchronize file contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data.
 *
 * Changed in version 2.2
 */
int fs_fsync(const char *path, int datasync, struct fuse_file_info *fi){
    printf("\nfs_fsync(path=\"%s\", datasync=%d, fi=0x%08x)\n", path, datasync, fi);
    
    // some unix-like systems (notably freebsd) don't have a datasync call
    if (datasync)
	    return check_retstat(fdatasync(fi->fh));
    else
	    return check_retstat(fsync(fi->fh));
}

/** Open directory
 *
 * This method should check if the open operation is permitted for
 * this  directory
 *
 * Introduced in version 2.3
 */
int fs_opendir(const char *path, struct fuse_file_info *fi){
    int retstat = 0;
    DIR *dp;
    char fpath[PATH_MAX];
    
    printf("\nfs_opendir(path=\"%s\", fi=0x%08x)\n", path, fi);
    fs_fullpath(fpath, path);

    // since opendir returns a pointer, takes some custom handling of
    // return status.
    dp = opendir(fpath);
    printf("opendir returned 0x%p\n", dp);
    if (dp == NULL){
	    perror("fs_opendir opendir");
        retstat = -errno;
    }
    
    fi->fh = (intptr_t) dp;
    
    return retstat;
}

/** Read directory
 *
 * This supersedes the old getdir() interface.  New applications
 * should use this.
 *
 * The filesystem may choose between two modes of operation:
 *
 * 1) The readdir implementation ignores the offset parameter, and
 * passes zero to the filler function's offset.  The filler
 * function will not return '1' (unless an error happens), so the
 * whole directory is read in a single readdir operation.  This
 * works just like the old getdir() method.
 *
 * 2) The readdir implementation keeps track of the offsets of the
 * directory entries.  It uses the offset parameter and always
 * passes non-zero offset to the filler function.  When the buffer
 * is full (or an error happens) the filler function will return
 * '1'.
 *
 * Introduced in version 2.3
 */

int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    DIR *dp;
    struct dirent *de;
    
    printf("\nfs_readdir(path=\"%s\", buf=0x%08x, filler=0x%08x, offset=%lld, fi=0x%08x)\n", path, buf, filler, offset, fi);
    // once again, no need for fullpath -- but note that I need to cast fi->fh
    dp = (DIR *) (uintptr_t) fi->fh;

    // Every directory contains at least two entries: . and ..  If my
    // first call to the system readdir() returns NULL I've got an
    // error; near as I can tell, that's the only condition under
    // which I can get an error from readdir()
    de = readdir(dp);
    printf("readdir returned 0x%p\n", de);
    if (de == 0) {
	    perror("fs_readdir readdir");
	    return -errno;
    }

    // This will copy the entire directory into the buffer.  The loop exits
    // when either the system readdir() returns NULL, or filler()
    // returns something non-zero.  The first case just means I've
    // read the whole directory; the second means the buffer is full.
    do {
	    // printf("calling filler with name %s\n", de->d_name);
	    if (filler(buf, de->d_name, NULL, 0) != 0) {
	        // printf("ERROR fs_readdir filler:  buffer full");
	        return -ENOMEM;
	    }
    } while ((de = readdir(dp)) != NULL);
    
    return 0;
}

/** Release directory
 *
 * Introduced in version 2.3
 */
int fs_releasedir(const char *path, struct fuse_file_info *fi){
    printf("\nfs_releasedir(path=\"%s\", fi=0x%08x)\n", path, fi);
    closedir((DIR *) (uintptr_t) fi->fh);
    
    return 0;
}

/**
 * Initialize filesystem
 *
 * The return value will passed in the private_data field of
 * fuse_context to all file operations and as a parameter to the
 * destroy() method.
 *
 * Introduced in version 2.3
 * Changed in version 2.6
 */
// Undocumented but extraordinarily useful fact:  the fuse_context is
// set up before this function is called, and
// fuse_get_context()->private_data returns the user_data passed to
// fuse_main().  Really seems like either it should be a third
// parameter coming in here, or else the fact should be documented
// (and this might as well return void, as it did in older versions of
// FUSE).
void *fs_init(struct fuse_conn_info *conn){
    return FS_DATA;
}

/**
 * Clean up filesystem
 *
 * Called on filesystem exit.
 *
 * Introduced in version 2.3
 */
void fs_destroy(void *userdata){
    return;
}

/**
 * Check file access permissions
 *
 * This will be called for the access() system call.  If the
 * 'default_permissions' mount option is given, this method is not
 * called.
 *
 * This method is not called under Linux kernel versions 2.4.x
 *
 * Introduced in version 2.5
 */
int fs_access(const char *path, int mask){
    int retstat = 0;
    char fpath[PATH_MAX];
   
    printf("\nfs_access(path=\"%s\", mask=0%o)\n", path, mask);
    fs_fullpath(fpath, path);
    
    retstat = access(fpath, mask);
    
    if (retstat < 0){
        perror("fs_access access");
	    return -errno;
    }
    
    return retstat;
}

/**
 * Create and open a file
 *
 * If the file does not exist, first create it with the specified
 * mode, and then open it.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the mknod() and open() methods
 * will be called instead.
 *
 * Introduced in version 2.5 
 */
int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi){
	int retstat = 0;
	char fpath[PATH_MAX];
	int fd;

	fs_fullpath(fpath, path);
	
	printf("creat(path\"%s\", mode=0%03o\n", fpath, mode);
	fd = creat(fpath, mode);
	if (fd < 0){
		perror("fs_create creat");
        retstat = -errno;
    }

	fi->fh = fd;
	return retstat;
}

/**
 * Change the size of an open file
 *
 * This method is called instead of the truncate() method if the
 * truncation was invoked from an ftruncate() system call.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the truncate() method will be
 * called instead.
 *
 * Introduced in version 2.5
 */
int fs_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi){
    int retstat = 0;
    
    printf("\nfs_ftruncate(path=\"%s\", offset=%ld, fi=0x%08x)\n", path, offset, fi);
    
    retstat = ftruncate(fi->fh, offset);
    if (retstat < 0){
	    perror("fs_ftruncate ftruncate");
        return -errno;
    }
    
    return retstat;
}

/**
 * Get attributes from an open file
 *
 * This method is called instead of the getattr() method if the
 * file information is available.
 *
 * Currently this is only called after the create() method if that
 * is implemented (see above).  Later it may be called for
 * invocations of fstat() too.
 *
 * Introduced in version 2.5
 */
int fs_fgetattr(const char *path, struct stat *statbuf, struct fuse_file_info *fi){
    int retstat = 0;
    
    printf("\nfs_fgetattr(path=\"%s\", statbuf=0x%08x, fi=0x%08x)\n", path, statbuf, fi);

    // On FreeBSD, trying to do anything with the mountpoint ends up
    // opening it, and then using the FD for an fgetattr.  So in the
    // special case of a path of "/", I need to do a getattr on the
    // underlying root directory instead of doing the fgetattr().
    if (!strcmp(path, "/"))
	    return fs_getattr(path, statbuf);
    
    retstat = fstat(fi->fh, statbuf);
    if (retstat < 0){
	    perror("fs_fgetattr fstat");
        return -errno;
    }
    
    return retstat;
}

static struct fs_operations: public fuse_operations {
	fs_operations(){
        getattr = fs_getattr;
        readlink = fs_readlink;
        getdir = NULL;
        mknod = fs_mknod;
        mkdir = fs_mkdir;
        unlink = fs_unlink;
        rmdir = fs_rmdir;
        symlink = fs_symlink;
        rename = fs_rename;
        link = fs_link;
        chmod = fs_chmod;
        chown = fs_chown;
        truncate = fs_truncate;
        utime = fs_utime;
        open = fs_open;
        read = fs_read;
        write = fs_write;
        statfs = fs_statfs;
        release = fs_release;
        fsync = fs_fsync;
        opendir = fs_opendir;
        readdir = fs_readdir;
        releasedir = fs_releasedir;
        init = fs_init;
        destroy = fs_destroy;
        access = fs_access;
        ftruncate = fs_ftruncate;
        fgetattr = fs_fgetattr;
	}
} operations;

void fs_usage(){
    fprintf(stderr, "usage:  ./filesystem [FUSE and mount options] rootDir mountPoint\n");
    abort();
}

int main(int argc, char *argv[]){
    int fuse_stat;
    struct fs_state *fs_data;

    // fsfs doesn't do any access checking on its own (the comment
    // blocks in fuse.h mention some of the functions that need
    // accesses checked -- but note there are other functions, like
    // chown(), that also need checking!).  Since running fsfs as root
    // will therefore open Metrodome-sized holes in the system
    // security, we'll check if root is trying to mount the filesystem
    // and refuse if it is.  The somewhat smaller hole of an ordinary
    // user doing it with the allow_other flag is still there because
    // I don't want to parse the options string.
    if ((getuid() == 0) || (geteuid() == 0)) {
    	fprintf(stderr, "Running fsFS as root opens unnacceptable security holes\n");
    	return 1;
    }

    // See which version of fuse we're running
    fprintf(stderr, "Fuse library version %d.%d\n", FUSE_MAJOR_VERSION, FUSE_MINOR_VERSION);
    
    // Perform some sanity checking on the command line:  make sure
    // there are enough arguments, and that neither of the last two
    // start with a hyphen (this will break if you actually have a
    // rootpoint or mountpoint whose name starts with a hyphen, but so
    // will a zillion other programs)
    if ((argc < 3) || (argv[argc-2][0] == '-') || (argv[argc-1][0] == '-'))
	    fs_usage();

    fs_data = (struct fs_state *) malloc(sizeof(struct fs_state));
    if (fs_data == NULL) {
	    perror("main calloc");
	    abort();
    }

    // Pull the rootdir out of the argument list and save it in my
    // internal data
    fs_data->rootdir = realpath(argv[argc-2], NULL);
    argv[argc-2] = argv[argc-1];
    argv[argc-1] = NULL;
    argc--;
    
    // turn over control to fuse
    fprintf(stderr, "about to call fuse_main\n");
    fuse_stat = fuse_main(argc, argv, &operations, fs_data);
    fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
    
    return fuse_stat;
}
