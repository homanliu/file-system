# File System

## Reference

### Documentation

* [libfuse API Documentation](https://libfuse.github.io/doxygen/index.html)

### Paper

* [Container Library and FUSE Container File System - Universität Hamburg](https://wr.informatik.uni-hamburg.de/_media/research/labs/2008/2008-03-michael_kuhn-container_library_and_fuse_container_file_system-report.pdf)

### Tutorials

* [Develop your own filesystem with FUSE - IBM](https://www.ibm.com/developerworks/linux/library/l-fuse/)
* [Writing a FUSE Filesystem: a Tutorial - New Mexico State University](https://www.cs.nmsu.edu/~pfeiffer/fuse-tutorial/)
* [Developing filesystems in userspace - Slides](https://www.slideshare.net/adorepump/fuse-developing-fillesystems-in-userspace)
* [Writing a Simple Filesystem Using FUSE in C - MQH Blog](http://www.maastaar.net/fuse/linux/filesystem/c/2016/05/21/writing-a-simple-filesystem-using-fuse/)
* [Operating Systems Principles FUSE - Humboldt University](http://sar.informatik.hu-berlin.de/teaching/2013-w/2013w_osp2/lab/Lab-4-FUSE/lab-FUSE_.pdf)
* [CS135 FUSE Documentation - Harvey Mudd College](https://www.cs.hmc.edu/~geoff/classes/hmc.cs135.201001/homework/fuse/fuse_doc.html)

### Code Reference

* [libfuse - GitHub](https://github.com/libfuse/libfuse)
* [Simple & Stupid Filesystem - MaaSTaaR](https://github.com/MaaSTaaR/SSFS)
* [fuse-google-drive - jcline](https://github.com/jcline/fuse-google-drive)
* [helloworld-fuse - JulesWang](https://github.com/JulesWang/helloworld-fuse)